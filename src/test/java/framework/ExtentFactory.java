package framework;

import com.relevantcodes.extentreports.ExtentReports;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.String.format;

public class ExtentFactory {
	public static String FolderName;
	public static String Package;
	public static String testName;

	public static ExtentReports getInstance(Object testClass)
	{		System.out.println(format("%1s Started", testClass.getClass().getSimpleName()));
		// Date appended to Report.html
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM_dd_hh_mm_ss");
		FolderName = testClass.getClass().getPackage().getName();
		Package    = simpleDateFormat.format(new Date()) + "_" +testClass.getClass().getPackage().getName();
		testName   = testClass.getClass().getSimpleName();

		String Path = format("test-output/Extent-Report/%1s/%2s_%3s.html",FolderName, Package,testName);

		ExtentReports extent;
		extent = new ExtentReports(Path, false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"/extent-config.xml"));
		extent	.addSystemInfo("Host Name"	, "QA AUTOMATION TEAM")
				.addSystemInfo("Environment"	, "QA/TEST")
				.addSystemInfo("User Name"	, "Muhammad Ali");

		return extent;

	}
}
