package Project.OrangeHRM.test;
import com.relevantcodes.extentreports.LogStatus;
import Project.OrangeHRM.Base.GlobleConfig;
import org.testng.Assert;
import framework.DriverSingleton;
import Project.OrangeHRM.Base.Supertest;
import org.testng.annotations.*;


public class loginTest extends Supertest {

    DriverSingleton ds = new DriverSingleton();

    @Test(priority = 1, description = "Login With valid Credential")
    public void User_Login_With_Correct_User_Correct_Password() throws Exception
    {
        driver.get(GlobleConfig.BASEURL);
        logger = report.startTest("Login With valid Credential");

        logger.log(LogStatus.INFO, "Enter UserName.");
        _commonUtils.leftClickElement(_LoginPage.Username);
        _commonUtils.sendKeyOnElement(_LoginPage.Username,"Admin");

        logger.log(LogStatus.INFO, "Enter Password.");
        _commonUtils.leftClickElement(_LoginPage.Password);
        _commonUtils.sendKeyOnElement(_LoginPage.Password,"admin123");


        logger.log(LogStatus.INFO, "Press Login.");
        _commonUtils.leftClickElement(_LoginPage.btn_Submit_Login);
        Assert.assertEquals(driver.getTitle(),"OrangeHRM2");



        ds.analyzeLog();

    }



}























/*
    @Test
    @Parameters("myName")
    public void parameterTest(String myName)
    {
       System.out.println("Parameterized value is : " + myName);
    }
*/


