package Project.OrangeHRM.Base;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import Project.OrangeHRM.pages.LoginPage;
import Project.OrangeHRM.pages.Sample;
import helper.CommonUtils;
import helper.Constants;
import framework.ExtentFactory;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import com.relevantcodes.extentreports.*;
import framework.DriverSingleton;
import org.testng.asserts.SoftAssert;

public class Supertest {

    // Pages fields declaration:
    protected CommonUtils _commonUtils;
    protected GlobleConfig _globleConfig;
    // protected Reportmail                _reportmail ;

    // Pages fields declaration:
    protected Sample _Sample;
    protected LoginPage _LoginPage;
    protected SoftAssert                SoftAssert;

    // Reports fields:
    protected ExtentReports     report;
    public static ExtentTest    logger;



    // Other fields:

    protected String testCaseStatus = "PASSED";
    public WebDriver driver = DriverSingleton.setDriver();
    protected long time;
    DriverSingleton ds = new DriverSingleton();

    @BeforeClass(alwaysRun = true)
    public void beforeClassSetup()
    {
        try
        {
            // Helper fields initialization:
             SoftAssert               = new SoftAssert                   ();
            _commonUtils              = new CommonUtils                  (driver);
            _globleConfig             = new GlobleConfig                 ();
            // _reportmail             = new Reportmail                  ();

            // Pages fields initialization:
            _Sample  =new  Sample(driver);
            _LoginPage = new LoginPage(driver);

            ds.analyzeLog();
            driver.get(GlobleConfig.BASEURL);
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            // Set the deployment server
            report = ExtentFactory.getInstance(this);


        }
        catch (Exception e)
        {
            e.getMessage();
        }


    }

    @AfterMethod
    public void screenCap(ITestResult result)
    {
        try
        {
            System.out.println("AfterMethod");
            if (result.getStatus() == ITestResult.FAILURE)
            {
                testCaseStatus = "FAILED";
                System.out.println(System.getProperty("user.dir"));
                String screenshot_path = Constants.captureScreenshot(result.getTestClass().getName());
                String imagePath = logger.addScreenCapture(screenshot_path);
                logger.log(LogStatus.FAIL,  this.getClass().getSimpleName() , imagePath);
                logger.log(LogStatus.ERROR,result.getThrowable());

            }
            else if(result.getStatus()==ITestResult.SUCCESS)
            {
                logger.log(LogStatus.PASS, this.getClass().getSimpleName() + " Test Case Success and Title Verified");
            }
            else if(result.getStatus()==ITestResult.SKIP)
            {
                logger.log(LogStatus.SKIP, this.getClass().getSimpleName() + " Test Case Skipped");
            }
        }

        catch (Exception e)
        {
            e.getMessage();
        }
    }



    @AfterClass(alwaysRun = true)
    public void tearDown()
    {
        try {
            System.out.println("Tearing down test " + getClass().getName() + "....");
            logger = report.startTest("Start tear down.");
            _commonUtils.waitFor(Duration.ofSeconds(2));
            logger.log(LogStatus.INFO, "Clear cache.");
            Constants.deleteAllBrowserCookies();
            logger.log(LogStatus.INFO, "Tear Down Successfully.");
            report.endTest(logger);
            report.flush();
            time = System.currentTimeMillis();
        }
        catch (Exception e)
        {
            e.getMessage();
        }


    }

    @AfterSuite(alwaysRun = true)
    public void closeBrowser()
    {
        try
        {
            driver.close();
            driver.quit();
        }
        catch (Exception e)
        {
            e.getMessage();
        }

    }
}
